const Order = require("../models/Order");
const Product = require("../models/Product");
const mongoose = require("mongoose");


// create order
module.exports.createOrder = async (req,res) => {

    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden."})
    } else {
       
        // Auto compute total amount of order
        const productIds = req.body.products.map(product => (product.productId));
        const products = await Product.find({'_id': {$in: productIds}})

        let totalAmount = 0;
        products.forEach((product,index) => {

            totalAmount += product.price * req.body.products[index].quantity
            
        })
        
        // console.log(totalAmount)

        let newOrder = new Order({

            totalAmount: totalAmount,
            userId: req.user.id,
            products: req.body.products

        })

        newOrder.save()
        .then(result => res.send({message: "Order Successful."}))
        .catch(error => res.send(error))
    }
}



module.exports.retrieveOrders = (req,res) => {
    Order.find({userId: req.user.id})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.retrieveAllOrders = (req,res) => {

    Order.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.orderContents = (req,res) => {
    // console.log(req.params.id)
    Order.findById({_id: req.params.orderId}, {products: 1})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}