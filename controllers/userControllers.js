const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (req,res) => {

    const hashedPw = bcrypt.hashSync(req.body.password,10)
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
        email: req.body.email,
        password: hashedPw
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.loginUser = (req,res) => {

    User.findOne({email: req.body.email})
    .then(foundUser =>{
        if(foundUser === null){
            return res.send({message: "User Not Found."});
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)

            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(foundUser)})
            } else {
                return res.send({message: "Incorrect Password"})
            }
        }
    })

}

// get user's details - must be logged in
module.exports.getUserDetails = (req,res) => {

    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}



// set user as an admin
module.exports.setUserAsAdmin = (req,res) => {

    if(req.params.isAdmin){
        return res.send({message: "User is Already an Admin."})
    } else {
        let updates = {
            isAdmin: true
        }

        User.findByIdAndUpdate(req.params.userId,updates,{new: true})
        .then(result => res.send(result))
        .catch(error => res.send(error))
    }

}

