const Product = require("../models/Product");


module.exports.getActiveProducts = (req,res) => {

    Product.find({isActive:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.getSingleProduct = (req,res) => {
    // console.log(req.params);
    Product.findById(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.createProduct = (req,res) => {
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    })

    newProduct.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
}

module.exports.updateProduct = (req,res) => {

    let updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Product.findByIdAndUpdate(req.params.productId,updates,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.archiveProduct = (req,res) => {

    let updates = {
        isActive: false
    }

    Product.findByIdAndUpdate(req.params.productId,updates,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.activateProduct = (req,res) => {

    let updates = {
        isActive: true
    }

    Product.findByIdAndUpdate(req.params.productId,updates,{new: true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

module.exports.productsPerOrder = (req,res) => {

    Order.findById({orderId: req.params.orderId})
    .then(res.send(req.params.products))
    .catch(error => res.send(error))

}