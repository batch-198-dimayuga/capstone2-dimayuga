const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const{verify,verifyAdmin} = auth;

// get Active products
router.get('/active',productControllers.getActiveProducts);

// get Single Product
router.get("/getSingleProduct/:productId",productControllers.getSingleProduct);

// create product
router.post('/',verify,verifyAdmin,productControllers.createProduct);

// update product
router.put('/updateProduct/:productId',verify,verifyAdmin,productControllers.updateProduct);

// archive product
router.delete('/archiveProduct/:productId',verify,verifyAdmin,productControllers.archiveProduct)

// activate product
router.put('/activateProduct/:productId',verify,verifyAdmin,productControllers.activateProduct);

// get products per order
router.get('/products/:orderId',verify,verifyAdmin,productControllers.productsPerOrder)

module.exports = router;