const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// register user
router.post("/",userControllers.registerUser);

// login user
router.post('/login',userControllers.loginUser);

// get user logged in details 
router.get('/getUserDetails',verify,userControllers.getUserDetails);

// set user as admin
router.put('/updateAdmin/:userId',verify,verifyAdmin,userControllers.setUserAsAdmin);



module.exports = router;