const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// create order
router.post('/checkout',verify,orderControllers.createOrder);

// retrieve authenticated user's orders
router.get('/',verify,orderControllers.retrieveOrders);

// retrieve all orders
router.get('/allOrders',verify,verifyAdmin,orderControllers.retrieveAllOrders);

//retrieve products in an order
router.get('/:orderId/products',verify,orderControllers.orderContents)

module.exports = router;