const express = require("express");
const app = express();
const port = 4000;
const mongoose = require("mongoose");

mongoose.connect("mongodb+srv://admin:admin123@cluster0.azr3rex.mongodb.net/ecommerceAPI?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true 
});

let db = mongoose.connection;
db.on('error',console.error.bind(console, "MongoDB Connection Error."));

db.once('open',()=>console.log("Connected to MongoDB."))

app.use(express.json());

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes);

const orderRoutes = require("./routes/orderRoutes");
app.use("/orders",orderRoutes);


app.listen(port,()=>console.log("Server is running at localhost:4000"));