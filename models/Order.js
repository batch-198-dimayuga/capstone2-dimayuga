const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({

    totalAmount: {
        type: Number,
        required: [true, "Total Amount is required"]
    },
    purchasedOn: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: String,
        required: [true, "User Id is required"]
    },
    products: [
        {
            productId: {
                type: String,
                required: [true, "Product Id is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required"]
            }
        }
    ]

})

module.exports = mongoose.model("Order",orderSchema);